# PONG.ASM
## A Pong clone for MS-DOS built using the TASM assembler
### by @DavidKaluta
A Pong game built 100% in *assembly*.

Press Esc to leave to the MS-DOS prompt.

W/S control left paddle.

Up/Down control right paddle.

## Screenshots
![Title Screen](https://i.imgur.com/c2YyQZD.png)
![Game](https://i.imgur.com/EZw6WC1.png)
![Victory Screen](https://i.imgur.com/868y0lP.png)
